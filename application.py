# This Python file uses the following encoding: utf-8
import sys, os, importlib, pkgutil
import numpy as np
import xlsxwriter
from PySide2 import QtCore, QtGui, QtWidgets

from matplotlib.backends.qt_compat import QtCore, QtWidgets
from matplotlib.backends.backend_qt5agg import (FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

import application_rc

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, devices):
        super(MainWindow, self).__init__()

        self.curFile = '' # This is the file path for the excel spreadsheet.
        self.curDataSets = []
        self.curWorksheet = 0
        self.isModified = False
        self.devices= devices
        self.device = False

        self.createActions()
        self.createMenus()
        self.createToolBars()
        self.createStatusBar()

        self.setWindowTitle("PyPhotoSpectrometer")
        self.setUnifiedTitleAndToolBarOnMac(True)

    def closeEvent(self, event):
        if self.maybeSave():
            event.accept()
        else:
            event.ignore()

    def createActions(self):
        # File Actions
        self.newAct = QtWidgets.QAction(QtGui.QIcon(':/images/new.png'), "&New",
                self, shortcut=QtGui.QKeySequence.New,
                statusTip="Create a new file", triggered=self.newFile)

        self.saveAct = QtWidgets.QAction(QtGui.QIcon(':/images/save.png'),
                "&Save", self, shortcut=QtGui.QKeySequence.Save,
                statusTip="Save the document to disk", triggered=self.save)

        self.saveAsAct = QtWidgets.QAction("Save &As...", self,
                shortcut=QtGui.QKeySequence.SaveAs,
                statusTip="Save the document under a new name",
                triggered=self.saveAs)

        self.exitAct = QtWidgets.QAction("E&xit", self, shortcut="Ctrl+Q",
                statusTip="Exit the application", triggered=self.close)

        # Device Actions
        self.manageDevices = QtWidgets.QAction("&Manage Devices", self, shortcut="Ctrl+M",
                statusTip="Manage connected devices", triggered=self.deviceMgrDialog)

        # Scan Actions
        # Not sure if we need this. It is probably going to be dynamic and linked to the device itself!


    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.newAct)
        self.fileMenu.addAction(self.saveAct)
        self.fileMenu.addAction(self.saveAsAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)

        self.deviceMenu = self.menuBar().addMenu("&Devices")
        self.deviceMenu.addAction(self.manageDevices)

        self.scanMenu = self.menuBar().addMenu("&Scan")
        # self.scanMenu.addAction(self.device.baseline)


    def createToolBars(self):
        self.fileToolBar = self.addToolBar("File")
        self.fileToolBar.addAction(self.newAct)
        self.fileToolBar.addAction(self.saveAct)

    def createStatusBar(self):
        self.statusBar().showMessage("Ready")

    # ACTIONS
    def newFile(self):
        # Clear out all info
        self.curFile = ''
        self.curDataSets = []
        self.curWorksheet = 0
        self.isModified = False

    def save(self):
        if self.curFile:
            return self.saveExcel(self.curFile)
        return self.saveAs()

    def saveAs(self):
        fileName, filtr = QtWidgets.QFileDialog.getSaveFileName(self)
        # TODO: Add filter to for xlsx
        if fileName:
            self.curFile = fileName
            return self.saveExcel(self,fileName)
        return False

    def saveExcel(self,filename):
        workbook = xlsxwriter.Workbook(filename)
        for dataset in self.curDataSets:
            worksheet = workbook.add_worksheet(dataset['name'])
            # Header
            worksheet.write(0,0,"Y (" + data['yunits'] + ")")
            worksheet.write(0,1,"X (" + data['xunits'] + ")")

            # Y coordinates
            row = 1
            for y in (data['y']):
                worksheet.write(row,0,y)

            # X coordinates
            row = 1
            for x in (data['x']):
                worksheet.write(row,1,x)

            # Line Chart
            chart = workbook.add_chart({'type':'line'})
            chart.add_series({
                'name': dataset['name'],
                'categories': dataset['name'] + '!$A$2:$A$' + dataset['y'].len,
                'categories': dataset['name'] + '!$B$2:$B$' + dataset['y'].len,
            })
            chart.set_title({'name':'Results of ' + dataset['name']})
            chart.set_x_axis({'name':dataset['xunits']})
            chart.set_y_axis({'name':dataset['yunits']})
            chart.set_style(10)
            worksheet.insert_chart('D2', chart)
        workbook.close()

    def maybeSave(self):
        if self.isModified:
            ret = QtWidgets.QMessageBox.warning(self, "Application",
                    "The data has not been saved.\nDo you want to save "
                    "your changes?",
                    QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Discard |
                    QtWidgets.QMessageBox.Cancel)
            if ret == QtWidgets.QMessageBox.Save:
                return self.save()
            elif ret == QtWidgets.QMessageBox.Cancel:
                return False
        return True

    def deviceMgrDialog(self):
        dialog = DeviceManagerDialog()
        dialog.show()

class DeviceManagerDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(DeviceManagerDialog, self).__init__(parent)

if __name__ == "__main__":
    # Load Devices
    devices = []
    deviceFolders = [dI for dI in os.listdir('devices') if os.path.isdir(os.path.join('devices',dI)) and not dI.startswith("_")]
    for folder in deviceFolders:
        mod = importlib.import_module('devices.'+folder)
        dev = getattr(mod,"device")()
        devices.append(dev)
        print("Loaded device named: " + dev.getDeviceTitle())

    # Start Window
    app = QtWidgets.QApplication(sys.argv)
    mainwin = MainWindow(devices)
    mainwin.show()
    sys.exit(app.exec_())
