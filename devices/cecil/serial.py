# Cecil Python Serial Library
# Includes parts from the Jcamp python library by Nathan Hagen, released under the MIT license.

import serial
import sys
import re
import time
from six import string_types
from numpy import array, linspace, alen, append, arange, logical_not, log10, nan

STX = 0x02
ETX = 0x03
ACK = 0x06
BEL = 0x07
NAK = 0x15
ACKout = b'\x06'
BELout = b'\x07'
NAKout = b'\x15'

SQZ_digits = {
    '@':'+0', 'A':'+1', 'B':'+2', 'C':'+3', 'D':'+4', 'E':'+5', 'F':'+6', 'G':'+7', 'H':'+8', 'I':'+9',
    ' ':'+0', 'a':'-1', 'b':'-2', 'c':'-3', 'd':'-4', 'e':'-5', 'f':'-6', 'g':'-7', 'h':'-8', 'i':'-9',
    '+':'+', ## For PAC
    '-':'-', ## For PAC
    ',':' ', ## For CSV
}
DIF_digits = {
    '%': 0, 'J':1,  'K':2,  'L':3,  'M':4,  'N':5,  'O':6,  'P':7,  'Q':8,  'R':9,
            'j':-1, 'k':-2, 'l':-3, 'm':-4, 'n':-5, 'o':-6, 'p':-7, 'q':-8, 'r':-9,
}
DUP_digits = {
    'S':1, 'T':2, 'U':3, 'V':4, 'W':5, 'X':6, 'Y':7, 'Z':8, 's':9,
}

def is_float(s):
    '''
    Test if a string, or list of strings, contains a numeric value(s).
    Parameters
    ----------
    s : str, or list of str
        The string or list of strings to test.
    Returns
    -------
    is_float_bool : bool or list of bool
        A single boolean or list of boolean values indicating whether each input can be converted into a float.
    '''

    if isinstance(s,tuple) or isinstance(s,list):
        if not all(isinstance(i, string_types) for i in s):
            raise TypeError("Input {} is not a list of strings".format(s))
        if (len(s) == 0):
            raise ValueError('Input {} is empty'.format(s))
        else:
            bool = list(True for i in range(0,len(s)))
            for i in range(0,len(s)):
                try:
                    float(s[i])
                except ValueError:
                    bool[i] = False
        return(bool)
    else:
        if not isinstance(s, string_types):
            raise TypeError("Input '%s' is not a string" % (s))

        try:
            float(s)
            return(True)
        except ValueError:
            return(False)

def get_value(num, is_dif, vals):
    n = float(num)
    if is_dif:
        lastval = vals[-1]
        val = n + lastval
    else:
        val = n

    return(val)

def jcamp_parse(line):
    line = line.strip()

    datavals = []
    num = ""
    newline = list(line[:])
    offset = 0
    for (i,c) in enumerate(line):
        if (c in DUP_digits):
            prev_c = line[i-1]
            mul = DUP_digits[c]
            newline.pop(i + offset)
            for j in range(mul - 1):
                newline.insert(i + offset, prev_c)
            offset += mul
    line = "".join(newline)

    DIF = False
    for c in line:
        if c.isdigit() or (c == "."):
            num += c
        elif (c in SQZ_digits):
            DIF = False
            if num:
                n = get_value(num, DIF, datavals)
                datavals.append(n)
            num = SQZ_digits[c]
        elif (c in DIF_digits):
            if num:
                n = get_value(num, DIF, datavals)
                datavals.append(n)
            DIF = True
            num = str(DIF_digits[c])
        else:
            raise Exception("Unknown caracter (%s) encountered while parsing data" % c)

    if num:
        n = get_value(num, DIF, datavals)
        datavals.append(n)
    return datavals

class serial:
    def __init__(self,cport=None):
        # Initialization of variables (for GUI state machines)
        self.ser = None
        self.currentCell = None
        self.baseline = None
        self.ready = False
        self.fault = False
        self.changing = False
        self.printing = False
        self.cellchange = False
        self.ready2 = False
        self.uvstatus = 'Offline'
        self.task = False
        self.motoracts = False
        self.calibrationstatus = False
        self.accessory = False
        self.cells = 1
        self.tempcontrol = False
        # Now for the serial initialization stuff. If we are not connecting right away, then
        if cport == None:
            return      
        self.open_port(cport)
    def __del__(self):
        if self.ser:
            self.ser.close()
    def get_ports(self):
        # From https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port,rts=True,dtr=True)
                # Check if DTR is on
                if s.cts and s.dsr and s.cd:
                    result.append(port)
                s.close()
            except (OSError, serial.SerialException):
                pass
        return result
    def open_port(self,cport):
        self.ser = serial.Serial(cport,9600, timeout=None, parity=serial.PARITY_EVEN, bytesize=serial.SEVENBITS, stopbits=2)
        # First check: CTS,DCD,DSR
        self.ser.rts = True
        self.ser.dtr = True
        if not (self.ser.cts and self.ser.dsr and self.ser.cd):
            print("Incorrect serial port, wrong serial cable, or device not on")
            raise Exception("Serial port error")
        # Second check: short status
        self.flush()
        self.updateGenStatus()

    ######################### Functions for all commands #############
    def write(self,string):
        if self.ser is None:
            raise Exception("Port not Open")
        print("Write: ",repr(string))
        self.ser.write(string.encode('ascii'))
    def readShort(self,timeout=5):
        # This function expects a ack, nak, or bel and returns 0, 1, 2 respectively
        # From section 3.1 in the serial manual
        if self.ser is None:
            raise Exception("Port not Open")
        self.ser.timeout = timeout
        byte = self.ser.read()[0]
        if byte is ACK:
            return 0
        elif byte is NAK:
            return 1
        elif byte is BEL:
            return 2
        else:
            return -1

    def readLong(self):
        # Will read a string like the following: stx, status, data-type, data-bytes, etx
        # From Section 3.2 in the manual
        if self.ser is None:
            raise Exception("Port not Open")
        result = bytearray()
        notdone = True
        # TODO: Need to handle empty
        try:
            mybyte = self.ser.read()[0]
        except:
            raise Exception("Spectrometer not responding")
        if mybyte != STX:
            raise Exception("First byte not start transmission!")

        # Status should be the second byte
        self.status = self.ser.read()[0]
        self.convertStatusByte(self.status)

        # We will give the rest back, including the data type for the calling function to verify
        while True:
            mybyte = self.ser.read()[0]
            result.append(mybyte)
            if mybyte is ETX:
                return result.decode('ascii')
        

    def readJcamp(self,unit="A"): 
        # Averaging is the float param for SCU!
        # Dictionary Based off of pypi.org/project/jcamp
        if self.ser is None:
            raise Exception("Port not Open")
        result = {}
        result['xjump'] = None
        y = []
        x = []
        datastart = False
        self.ser.timeout = 500
        while True:
            line = self.ser.readline().decode('ascii')
            print("Recieved Line: ",line)
            
            if line.startswith('$$'):
                self.ser.write(ACKout)
                continue
            if line.startswith('##'):
                line = line.strip('##')
                (lhs,rhs) = line.split('=',1)
                lhs = lhs.strip().lower()
                rhs = rhs.strip()

                if rhs.isdigit():
                    result[lhs] = int(rhs)
                elif is_float(rhs):
                    result[lhs] = float(rhs)
                else:
                    result[lhs] = rhs

                if (lhs in ('xydata','xypoints','peak table')):
                    ## This is a new data entry, reset x and y. 
                    print("New data entry")
                    x = []
                    y = []
                    datastart = True
                    datatype = rhs
                    self.ser.write(ACKout)
                    continue        ## data starts on next line
                elif (lhs == 'end'):
                    #bounds = [int(i) for i   in re_num.findall(rhs)]
                    #datastart = True
                    #datatype = bounds
                    #datalist = []
                    #self.ser.write(ACKout)
                    #continue
                    result["x"] = x
                    result["y"] = y
                    if unit == "A":
                        result['yunits'] = "ABSORBANCE"
                    return result
                elif datastart:
                    datastart = False
                self.ser.write(ACKout)
                continue
            elif line.startswith('     '): # Noticed that the instrument parameters start with five spaces
                line = line.strip('     ')
                (lhs,rhs) = line.split('=',1)
                # Don't know what to do with this
                self.ser.write(ACKout)
                continue
            if datastart and (datatype == '(X++(Y..Y))'): # This is what the cecil uses
                # At this point, we will have the following: xfactor, yfactor, firstx, lastx, *npoints*,dataprocessing=averaging: 2.5 nm
                # X is in nanometers, Y is in "transmittance"
                if result['xjump'] is None: # Calculate
                    result['xjump'] = (result['lastx'] - result['firstx']) / (result['npoints']-1)
                line = jcamp_parse(line) # Converts the line into an array since the data could have minuses or spaces as delimiters
                xcur = float(line[0])
                for i in range(1,len(line)-1):
                    if unit == "T%":
                        y.append(float(line[i]) * result['yfactor'])
                    elif unit == "A":
                        y.append(2 - log10(float(line[i]) * float(result['yfactor']) * 100))
                    x.append((xcur + ((i-1)*result['xjump'])) * result['xfactor'])
            if datastart and not (datatype == '(X++(Y..Y))'):
                raise Exception("Unknown datatype")
            self.ser.write(ACKout)
                
            

    
    def flush(self):
        if self.ser is None:
            raise Exception("Port not Open")
        # Clears out serial buffers on both host and device
        self.ser.reset_input_buffer()
        self.ser.reset_output_buffer()
        self.write("@")
    def commandInstant(self,command,flush=False):
        if flush:
            self.flush()
        self.write("?" + command + "\r")
        return self.readShort()
    def commandAckWait(self,command,flush=False):
        if flush:
            self.flush()
        self.write("?" + command + "\r")
        return self.readShort(300)
    def commandLong(self,command,flush=False):
        if flush:
            self.flush()
        self.write("?" + command + "\r")
        return self.readLong()

    # Smart Command
    #def command(self,command,**kwargs):
        # 1:instant,2:wait for report,3:wait for ack,4:get longer return,
    #    instantCommands = ('cal','d2n','glv','off','rng','spm','spv','tps','amn','amx','bwt','cmn','cmx','exp','glb','gle','mmx','mrm','mrn','scs','scu','sdd','ssp','ssx','stp','tcb','tce','tcs','tmn','tmx','wle','wls')
    #    reportWaitCommands = ('gls','azo','pbl','pek','bas','scn') # Require RPS
    #    ackWaitCommands = ('dga','dgb','dgr','gto','ccf','ccr')
    #    longRetCommands = ('sds','bds')
    #    jcampCommands   = ('sdu')



    ############################# General #############################
    def gotoWavelength(self,wl):
        # Resolution max of 0.1
        wl = str(round(wl,1))
        self.changing = True
        ret = self.commandAckWait("GTO" + wl,True)
        self.changing = False
        return ret

    def updateGenStatus(self,flush=True):
        self.ser.write(b"?RPS\r") # Should be like <start><status>@RKON        <end>
        self.ser.timeout = 5 # Seconds
        statstring = self.readLong() 
        # print("Read: ",repr(statstring))
        # Type Byte validation
        if statstring[0] is not '@':
            raise Exception("Improper return recieved")
        # Machine status (also in status byte)
        self.ready2 = statstring[1] is 'R'
        # Availability of the Duterium Lamp
        if statstring[2] is 'K': self.uvstatus = "UV Ready"
        elif statstring[2] is 'k': self.uvstatus = 'Vis Ready'
        elif statstring[2] is 'L': self.uvstatus = 'D2 Lamp Failed'
        elif statstring[2] is 'H': self.uvstatus = 'D2 Lamp Heating'
        else: raise Exception("Improper availability byte from RPS")
        #TODO: UV Status should be set to D2 off after D2N command
        # Printer Check (F,O)
        self.printerstatus = statstring[3] is 'O'
        # Current task
        tasks = {
           "A":"Auto-Zeroing",
           "E":"Peak Seeking",
           "B":"Baseline", # or peak seek baseline
           "g":"Gel scan",
           "S":"Scannig",
           "N":False
        }
        self.task = tasks[statstring[4]]
        # Motor Activity
        motoracts = {
            "L":"Lamp Change",
            "F":"Filter Change",
            "R":"Attenuator Change",
            " ":False
        }
        self.motorstatus = motoracts[statstring[5]]
        # Self-Calibration Status: Will be used for an exception probably
        calibstat = {
            "B":"peak seek failed because baseline absent",
            "E":"baseline faled",
            "F":"no peak found",
            "X":"peak seek failed (over range/lid open)",
            "z":"machine unable to autozero",
            " ":False
        }
        self.calibrationstatus = calibstat[statstring[6]]
        #Accessories: 8 (C,G,S,s,A)
        accessories = {
            "C":"Cell changer",
            "G":"Gel scanner",
            "S":"return sipette",
            "s":"SIP pump or batch sampling pump",
            "A":"Unknown accessory",
            " ":False
        }
        self.accessory = accessories[statstring[8]]
        #Cell Changer: 9
        if statstring[9].isdigit():
            self.cells = statstring[9]
        else:
            self.cells = 1
        #temp controller: 10
        self.tempcontrol = statstring[12] is 'T'
        
    def printStatus(self):
        self.updateGenStatus()
        print("Photometer Ready: ",self.ready)
        print("Photometer Ready check 2: ",self.ready2)
        print("Photometer Fault: ",self.fault)
        print("Changing Filter/attenuator: ",self.changing)
        print("Print Command Flag: ",self.printing)
        print("Changing Cells: ",self.cellchange)

    def convertStatusByte(self,item):
        # Gets status
        # Bit # Meaning
        # ##################
        # 7	  # Parity
        # 6	  # Always 1 (40)
        # 5	  # photometer ready = 1 (20)
        # 4	  # Fault = 1 (10)
        # 3	  # Unused
        # 2	  # Lamp filter/attenuator change in progress (04)
        # 1	  # print command flag set (02)
        # 0	  # cell change in progress (01)
        if not ((item & 0x40) is 0x40):
            raise Exception("Status Byte invalid")
        self.ready  = (item & 0x20) == 0x20
        self.fault  = (item & 0x10) == 0x10
        self.changing  = (item & 0x04) == 0x04
        self.printing  = (item & 0x02) == 0x02
        self.cellchange  = (item & 0x01) == 0x01


    ######################### Cell Changer ############################
    def gotoCell(self,cell):
        if self.cells is False:
            return
        if cell > self.cells:
            raise Exception("Requested cell is greater than number of cells")

        # Initialize if not initialized
        if self.currentCell is None:
            if commandAckWait('CCR') is 0:
                self.currentCell = 1
            else:
                return

        if cell < self.currentCell:
            commandAckWait('CCR')
            self.currentCell = 1
        while cell is not self.currentCell: 
            commandAckWait('CCF')
            self.currentCell += 1
            if cell > self.cells:
                raise Exception("Bad Programmer: Requested cell is somehow greater than number of cells")
    
    def getCell(self):
        if self.currentCell is None: return 1
        else: return self.currentCell

    def scanCells(self,cells,start,stop,unit="A",speed=50,speedunit="s"):
        results = {}
        for cell in cells:
            self.gotoCell(cell)
            results[cell] = self.scanWavelengths(start,stop,unit,speed,speedunit)
        return results

        

    ############################## Scans ##############################
    def scanBaseline(self,start,stop,speed=50,speedunit="s"):
        self.flush()
        # 1. Goto starting WL
        self.gotoWavelength(start)
        # 2. Set start WL
        self.commandInstant("WLS"+str(round(start)))
        # 3. Set end WL
        self.commandInstant("WLE"+str(round(stop)))
        # 4. Set speed (SSP = in nm/s. Default of 50nm/s or 3000nm/min)
        if speedunit is "s":
            self.commandInstant('SSP'+str(round(speed,2))) # Default is 10nm/s, range: 1-50
        elif speedunit is "m":
            self.commandInstant('SCS'+str(round(speed))) # Default is 600, range: 1-4000
        else:
            raise Exception("Invalid Scan Speed Unit")
        self.commandInstant('BAS')
        self.updateGenStatus()
        while self.ready is not True:
            self.updateGenStatus()
            time.sleep(1) # Check every second
        if speedunit is 'nm/min': # Convert speedunit to nm/s for storage
            speed = speed * 60
        return self.getBaseline()
    
    def getBaseline(self):
        result = self.commandLong('BDS')
        print("Baseline result:",result)
        # start,end,resolution,potential resolution, speed
        result = [ float(i) for i in re.findall('[\d.]+',result)]
        if(len(result) is 1):
            raise Exception("Baseline information not available.")
            
        self.baseline = {
            'start':result[0],
            'stop':result[1],
            'resolution':result[2],
            'potentialresolution':result[3],
            'speed':result[4]/60.0 # speed is given in nm/min
        }
        return self.baseline

    def scanWavelengths(self,start,stop,**kwargs):
        # Suprisingly, it does not run the WL scan. Instead it uses the spectral functions
        # Process kwargs
        unit="A"
        speed=50
        speedunit="s"
        for key, value in kwargs.items():
            if key == "unit":
                unit = value
            elif key == "speed":
                speed = value
            elif key == "speedunit":
                speedunit == value
        # Validation of command parameters
        if start >= (stop + 1):
            raise Exception("Stop wavelength less than valid number")
        # Check if baseline is compatable
        if not isinstance(self.baseline,dict):
            raise Exception("Baseline not set")
        if self.baseline['start'] > start:
            raise Exception("Scan start outside of baseline start "+str(self.baseline['start']) + " " + str(start))
        if self.baseline['stop'] < stop:
            raise Exception("Scan end outside of baseline end. stored:"+str(self.baseline['stop']) + " requested:" + str(stop))
        if speedunit is 'm': # Convert speedunit to nm/s for storage
            speedsec = float(speed) * 60.0
        else:
            speedsec = float(speed)
        if self.baseline['speed'] < speedsec:
            raise Exception("Scan speed slower than baseline ("+str(self.baseline['speed'])+")")
        
        # 1. Goto starting WL
        self.flush()
        self.gotoWavelength(start)
        # 2. Set start WL
        self.commandInstant("WLS"+str(round(start)))
        # 3. Set end WL
        self.commandInstant("WLE"+str(round(stop)))
        # 4. Set speed (SSP = in nm/s. Default of 50nm/s or 3000nm/min)
        if speedunit is "s":
            self.commandInstant('SSP'+str(round(speed,2))) # Default is 10nm/s, range: 1-50
        elif speedunit is "m":
            self.commandInstant('SCS'+str(round(speed))) # Default is 600, range: 1-4000
        else:
            raise Exception("Invalid Scan Speed Unit")
        # 5. Verify that the settings are right with SDS
        sdsreturn = self.commandLong("SDS")
        print("SDS: ",sdsreturn)
        # TODO: convert sdsreturn into something useful from BLsxxxxexxxxrxxx.xxRxx.xx (x are numbers)
        # start,end,resolution,potential resolution
        sdsnumbers = [ float(i) for i in re.findall('[\d.]+',sdsreturn)]
        if len(sdsnumbers) is 1:
            resolution = sdsnumbers[0]
        else:
            resolution = sdsnumbers[3]
        # 6. Set unit (A, T%...)
        self.commandInstant('RNG'+unit.ljust(4))
        # 7. Send SCU along with the unit given by SDS (averaging unit should be equal to the accuracy)
        self.commandInstant('SCU'+str(round(resolution,2)))
        # 8. Read the ack and then the jcamp data
        jcamp = self.readJcamp()
        return jcamp
        # Note: only do flush at the beginning of this. Buck UV died RPTN twice before continuing. Not sure if that is needed.
    def stopScan(self):
        # Stop immediately, clearing out buffers
        self.ser.flush()
        self.write("?STP\r")
        # Do not know if there is a return ack or nack for this command
# Note on units
# wavenumber cm^-1 = 10,000,000/nm
# Absorbance = 2 - log(Transmittance in %)
