# This Python file uses the following encoding: utf-8
from PySide2 import QtWidgets
from .serial import serial

class device:
    def __init__(self):
        self.instance = False

    def __del__(self):
        if self.instance:
            self.instance.close()

    def getDeviceTitle(self):
        return "Cecil Serial Device"

    def getDeviceType(self):
        return "PS"

    def getDeviceSettings(self):
        # This will need to give the settings required
        return {}

    def getScanTypes(self):
        # This function gives the different types of scans and gives the functions to do the scans
        return {
            "baseline": {
                "title": "Base Line",
                "function": serial.baseline,
                "arguments": (
                    "Start",
                    "Stop",
                    "Speed"
                ),
                "output": False
            }
        }

    def initialize(self):
        self.instance = serial()
